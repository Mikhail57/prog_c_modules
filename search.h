//
// Created by mikhail-mustakimov on 06.04.18.
//

#ifndef MODULES_PROG_C_SEARCH_H
#define MODULES_PROG_C_SEARCH_H

#include "geometry_structs.h"
#include "vec_struct.h"
#include "geometry.h"

void search(Point *arr, size_t size, Vec3 &vec);

#endif //MODULES_PROG_C_SEARCH_H
