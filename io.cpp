//
// Created by mikhail-mustakimov on 05.04.18.
//

#include "io.h"

using namespace std;

int readData(char *filename, Point **arr, size_t &size) {
    ifstream input(filename);
    vector<Point> points;
    double x, y;
    input >> x >> y;
    while (!input.eof() && !input.fail()) {
        points.emplace_back(x, y);
        input >> x >> y;
    }
    if (input.fail())
        return -1;
    *arr = &points[0];
    size = points.size();
    input.close();
    return 0;
}

int writeTriangleData(char *filename, Triangle *triangles, size_t size) {
    ofstream output(filename);
    for (size_t i = 0; i < size; i++) {
        for (short j = 0; j < 3; j++)
            output << triangles[i].points[j].x << " " << triangles[i].points[j].y << " ";
        output << endl;
    }
    output.close();
    return 0;
}