//
// Created by mikhail-mustakimov on 06.04.18.
//

#include "vec_struct.h"

void Vec3::push_back(Triangle triangle, double perimeter) {
    for (size_t i = size - 1; i > 0; i--) {
        qualifier[i] = qualifier[i - 1];
        triangles[i] = triangles[i - 1];
    }
    qualifier[0] = perimeter;
    triangles[0] = triangle;

    sort();
}

void Vec3::sort() {
    for (size_t i = 0; i < size; i++) {
        for (size_t j = i + 1; j < size; j++) {
            if (qualifier[i] < qualifier[j]) {
                Triangle t_tmp = triangles[i];
                double p_tmp = qualifier[i];
                triangles[i] = triangles[j];
                qualifier[i] = qualifier[j];
                triangles[j] = t_tmp;
                qualifier[j] = p_tmp;
            }
        }
    }
}

Vec3::Vec3() {

}
