//
// Created by mikhail-mustakimov on 06.04.18.
//

#include "search.h"

void search(Point *arr, size_t size, Vec3 &vec) {
    for (size_t p1 = 0; p1 < size; p1++) {
        for (size_t p2 = p1 + 1; p2 < size; p2++) {
            for (size_t p3 = p2 + 1; p3 < size; p3++) {
                double perimeter = calculatePerimeter(arr[p1], arr[p2], arr[p3]);
                if (perimeter > vec.qualifier[2]) {
                    vec.push_back(Triangle(arr[p1], arr[p2], arr[p3]), perimeter);
                }
            }
        }
    }
}