//
// Created by mikhail-mustakimov on 06.04.18.
//

#ifndef MODULES_PROG_C_VEC_STRUCT_H
#define MODULES_PROG_C_VEC_STRUCT_H

#include <cstddef>
#include "geometry_structs.h"

struct Vec3 {
    Triangle triangles[3];
    double qualifier[3]{};
    size_t size = 3;

    Vec3();

    void push_back(Triangle triangle, double perimeter);

    void sort();
};

#endif //MODULES_PROG_C_VEC_STRUCT_H
