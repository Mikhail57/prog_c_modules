//
// Created by mikhail-mustakimov on 05.04.18.
//

#ifndef MODULES_PROG_C_GEOMETRY_H
#define MODULES_PROG_C_GEOMETRY_H

#include <cmath>
#include "geometry_structs.h"


double calcDistance(Point p1, Point p2);

double calculateArea(Point p1, Point p2, Point p3);

double calculatePerimeter(Point p1, Point p2, Point p3);

#endif //MODULES_PROG_C_GEOMETRY_H
