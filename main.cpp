#include <iostream>

#include "geometry_structs.h"
#include "vec_struct.h"
#include "geometry.h"
#include "io.h"
#include "search.h"


using namespace std;


int main() {
    Point *arr = nullptr;
    size_t size;
    readData("input", &arr, size);

    cout << arr << endl;
    cout.flush();

    Vec3 vec;

    search(arr, size, vec);

    writeTriangleData("output_p", vec.triangles, 3);

    return 0;
}
