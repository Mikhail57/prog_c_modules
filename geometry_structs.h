//
// Created by mikhail-mustakimov on 05.04.18.
//

#ifndef MODULES_PROG_C_POINT_H
#define MODULES_PROG_C_POINT_H

struct Point {
    double x;
    double y;

    Point();
    Point(double x, double y);
};

struct Triangle {
    Point *points;

    Triangle();
    Triangle(Point points[]);
    Triangle(Point p1, Point p2, Point p3);
};

#endif //MODULES_PROG_C_POINT_H
