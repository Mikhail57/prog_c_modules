//
// Created by mikhail-mustakimov on 06.04.18.
//

#include "geometry_structs.h"

/*
 * ***********************
 * Points struct functions
 * ***********************
 */

Point::Point() {
    this->x = 0;
    this->y = 0;
}

Point::Point(double x, double y) {
    this->x = x;
    this->y = y;
}


/*
 * *************************
 * Triangle struct functions
 * *************************
 */

Triangle::Triangle() {
    this->points = new Point[3];
}

Triangle::Triangle(Point points[]) {
    this->points = points;
}

Triangle::Triangle(Point p1, Point p2, Point p3) {
    this->points = new Point[3]{p1, p2, p3};
}