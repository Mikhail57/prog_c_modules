//
// Created by mikhail-mustakimov on 05.04.18.
//

#ifndef MODULES_PROG_C_IO_H
#define MODULES_PROG_C_IO_H

#include "geometry_structs.h"
#include <fstream>
#include <vector>

int readData(char *filename, Point **arr, size_t &size);

int writeTriangleData(char *filename, Triangle *triangles, size_t size);

#endif //MODULES_PROG_C_IO_H
