//
// Created by mikhail-mustakimov on 05.04.18.
//

#include "geometry.h"

inline double pow2(double a) {
    return a*a;
}

double calcDistance(Point p1, Point p2) {
    return sqrt(pow2(p1.x - p2.x) + pow2(p1.y - p2.y));
}

double calculateArea(Point p1, Point p2, Point p3) {
    return std::abs(p1.x*(p2.y - p3.y) + p2.x*(p3.y - p1.y) + p3.x*(p1.y - p2.y)) / 2;
}

double calculatePerimeter(Point p1, Point p2, Point p3) {
    return calcDistance(p1, p2) + calcDistance(p2, p3) + calcDistance(p3, p1);
}